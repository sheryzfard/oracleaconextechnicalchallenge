### Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Design Description](#design description)

### General info
This project is a simple Construction site Cleaning Simulation.
	
### Technologies
Project is created with:
* Java : 1.8
* maven : 3.5.4


### Setup
To run this project, build jar file using this command:

```
$ ./mvnw clean install
```
Then run the Main class with the map file address as an argument to Main using the below command:

```
$ java -cp site-clearing-1.0-SNAPSHOT.jar Main {your map file address}
```

### Design description

Mentioned below, classes that I extracted based on the domain background to design my implementation and the description of their behaviour .
######Block: 
This is an interface representing square blocks in the site map. It has its four subclasses; each one implements the behaviour of every type of existing land with the same name in the domain background. 
######PlainBLock, RockyBlock, BlockContainsClearableTree, and BlockContainsProtectedTree:
Respectively, implements Block as a plain land, Rocky land, the land containing tree that can be cleared and the Land containing a tree that must be preserved .

######SiteMap: 
The object of this class holds a 2D array of blocks which build the site map as a grid of block objects according to characterised map string receiving as argument of the method.
######Bulldozer: 
The object of this class represents the bulldozer characteristics, including its coordinates (X and Y) and direction. It implements three behaviours of a bulldozer; turn left, turn right and move forward and try to clean simultaneously depending on the head face and its location on the site map. 
######Direction:
An Enum including east, west, south, north and turn right and left behaviour.
######Activity:
An interface representing the event that occurs when the bulldozer is visiting or trying to clean a particular block of the site map. In different situations, its subclasses will return different costs or successful clearing status.
######ClearingPlainLand: 
It implements the behaviours of activity class when the bulldozer is trying to clear a plain land for the first time.
######ClearingRockyLand: 
It implements the behaviours of activity class when the bulldozer is trying to clear a rocky land for the first time.
######VisitingClearedLand: 
It implements the behaviours of activity class when the bulldozer is visiting a land that has been cleared already.
######ClearingLandWithClearableTree: 
It implements the behaviours of activity class when the bulldozer is stopping exactly on the land that contains a clearable tree and trying to clear it.
######PassingLandWithClearableTree:
It implements the behaviours of activity class when the bulldozer is trying to clear land that contains a clearable tree by passing through it.
######ClearingLandWithProtectedTree:
It implements the behaviours of activity class when the bulldozer is trying to clear land that contains a protected tree.
######ActivityCostManager: 
The object of this class is tasked to update a map of costs and items based on the activity happened or each time the simulator has to deliver a command to the bulldozer.
######TraineeCommandReader: 
The object of this class scans a command and translates it as a command type and movement number that can be passed to command executer.
######CommandExecuter: 
The object of this class does the right action, such as calling the proper method of bulldozer object, in case of any command type.
######CostItem:
An Enum including the items that cost different amounts including their credits.
######CostReporter:
The object of this class print cost report on console based on the map of CostItem and quantity, which is given as the argument of the method.
######Simulator:
This class instantiate objects of other classes and handle the procedure of simulation in the run method.



