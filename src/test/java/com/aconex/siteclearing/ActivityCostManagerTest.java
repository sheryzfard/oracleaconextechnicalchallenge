package com.aconex.siteclearing;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.activity.ClearingLandWithClearableTree;
import com.aconex.siteclearing.activity.ClearingPlainLand;
import com.aconex.siteclearing.activity.ClearingRockyLand;
import com.aconex.siteclearing.activity.PassingLandWithClearableTree;
import com.aconex.siteclearing.activity.VisitingClearedLand;
import com.aconex.siteclearing.activity.VisitingLandWithProtectedTree;
import com.aconex.siteclearing.cost.CostItem;
import com.aconex.siteclearing.exceptions.DestructionTreeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ActivityCostManagerTest {

    @Mock
    private Activity activity;
    @InjectMocks
    private ActivityCostManager activityCostManager = new ActivityCostManager(new HashMap<>(),100);

    @Test
    public void runActivityForClearingPlainLandUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.TRUE);
        when(activity.getCostMap()).thenReturn(new ClearingPlainLand().getCostMap());
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(1);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(99);
    }

    @Test
    public void runActivityForClearingRockyLandUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.TRUE);
        when(activity.getCostMap()).thenReturn(new ClearingRockyLand().getCostMap());
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(2);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(99);
    }


    @Test
    public void runActivityForClearingLandWithCLearableTreeUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.TRUE);
        when(activity.getCostMap()).thenReturn(new ClearingLandWithClearableTree().getCostMap());
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(2);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(99);
    }

    @Test
    public void runActivityForPassingLandWithCLearableTreeUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.TRUE);
        when(activity.getCostMap()).thenReturn(new PassingLandWithClearableTree().getCostMap());
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(2);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(99);
        assertThat(activityCostManager.getCostMap().get(CostItem.REPAIRING_PAINT_DAMAGE)).isEqualTo(1);
    }

    @Test
    public void runActivityForVisitingClearedLandUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.FALSE);
        when(activity.getCostMap()).thenReturn(new VisitingClearedLand().getCostMap());
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(1);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(100);
    }

    @Test(expected = DestructionTreeException.class)
    public void runActivityForVisitingLandWithProtectedTreeUpdateCostMap() {
        when(activity.isSuccessfulClearing()).thenReturn(Boolean.FALSE);
        when(activity.getCostMap()).thenReturn(new VisitingLandWithProtectedTree().getCostMap());
        doThrow(DestructionTreeException.class).when(activity).doAction();
        activityCostManager.runActivity(activity);
        assertThat(activityCostManager.getCostMap().get(CostItem.FUEL)).isEqualTo(1);
        assertThat(activityCostManager.getCostMap().get(CostItem.UNCLEARED_SQUARE)).isEqualTo(100);
    }
}