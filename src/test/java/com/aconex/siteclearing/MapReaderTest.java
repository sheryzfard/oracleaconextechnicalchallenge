package com.aconex.siteclearing;

import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MapReaderTest {
    private MapReader mapReader;

    @Before
    public void setUp() {
        mapReader = new MapReader();
    }

    @Test
    public void readMapShouldReturnMapAsStringListWhenMapFileExists() throws URISyntaxException {
        List<String> map = mapReader.readMap(this.getClass().getResource("map.txt").toURI());
        assertThat(map.get(0)).isEqualTo("ootooooooo");
    }

    @Test(expected = RuntimeException.class)
    public void readMapShouldThrowRuntimeExceptionWhenMapFileNotExists() throws URISyntaxException {
        assertThat(mapReader.readMap(this.getClass().getResource("no_map.txt").toURI()));
    }


}