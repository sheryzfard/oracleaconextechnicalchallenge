package com.aconex.siteclearing.command;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.Before;
import org.junit.Test;

public class TraineeCommandReaderTest {
    private TraineeCommandReader commandReader;

    @Before
    public void setUp() {
        commandReader = new TraineeCommandReader();
    }

    @Test
    public void readCommandShouldReturnCommandWithMoveForwardTypeAndMovementNumberWhenItIsAPlusNumber()  {
        assertThat( commandReader.readCommand("a 4").getCommandType()).isEqualTo(CommandType.MOVE_FORWARD);
        assertThat( commandReader.readCommand("a 4").getMovementNumber()).isEqualTo(4);
    }

    @Test
    public void readCommandShouldReturnCommandWithTurnRightTypeWhenItIsR()  {
        assertThat(commandReader.readCommand("r").getCommandType()).isEqualTo(CommandType.TURN_RIGHT);
    }

    @Test
    public void readCommandShouldReturnCommandWithTurnLeftTypeWhenItIsR()  {
        assertThat(commandReader.readCommand("l").getCommandType()).isEqualTo(CommandType.TURN_LEFT);
    }

    @Test
    public void readCommandShouldReturnCommandWithWrongCommandTypeWhenItIs() {
        assertThat(commandReader.readCommand("w").getCommandType()).isEqualTo(CommandType.WRONG_COMMAND);
    }

    @Test
    public void readCommandShouldThrowRuntimeExceptionWhenQ()  {
        assertThat(commandReader.readCommand("q").getCommandType()).isEqualTo(CommandType.QUIT);
    }
}