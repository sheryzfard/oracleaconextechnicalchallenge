package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class ClearingLandWithClearableTreeTest {

    private ClearingLandWithClearableTree clearingLandWithClearableTree;

    @Before
    public void setUp() {
        clearingLandWithClearableTree = new ClearingLandWithClearableTree();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(clearingLandWithClearableTree.isSuccessfulClearing()).isEqualTo(true);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfTwo() {
        assertThat(clearingLandWithClearableTree.getCostMap()).containsExactly(entry(CostItem.FUEL, 2));
    }
}