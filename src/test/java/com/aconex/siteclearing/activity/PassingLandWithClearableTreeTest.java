package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class PassingLandWithClearableTreeTest {

    private PassingLandWithClearableTree passingLandWithClearableTree;

    @Before
    public void setUp() {
        passingLandWithClearableTree = new PassingLandWithClearableTree();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(passingLandWithClearableTree.isSuccessfulClearing()).isEqualTo(true);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfTwoAndRepaingWithValueOne() {
        assertThat(passingLandWithClearableTree.getCostMap())
                .containsOnly(entry(CostItem.FUEL, 2), entry(CostItem.REPAIRING_PAINT_DAMAGE, 1));
    }
}