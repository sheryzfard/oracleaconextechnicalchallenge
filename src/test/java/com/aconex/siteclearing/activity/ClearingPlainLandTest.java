package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class ClearingPlainLandTest {

    private ClearingPlainLand clearingPlainLand;

    @Before
    public void setUp() {
        clearingPlainLand = new ClearingPlainLand();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(clearingPlainLand.isSuccessfulClearing()).isEqualTo(true);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfOne() {
        assertThat(clearingPlainLand.getCostMap()).containsExactly(entry(CostItem.FUEL, 1));
    }
}