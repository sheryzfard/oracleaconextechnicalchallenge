package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class VisitingLandWithProtectedTreeTest {

    private VisitingLandWithProtectedTree visitingLandWithProtectedTree;

    @Before
    public void setUp() {
        visitingLandWithProtectedTree = new VisitingLandWithProtectedTree();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(visitingLandWithProtectedTree.isSuccessfulClearing()).isEqualTo(false);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfTwoAndTreeDestructionOne() {
        assertThat(visitingLandWithProtectedTree.getCostMap())
                .containsOnly(entry(CostItem.FUEL, 1), entry(CostItem.PROTECTED_TREE_DESTRUCTION, 1));
    }
}