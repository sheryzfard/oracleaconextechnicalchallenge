package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class ClearingRockyLandTest {

    private ClearingRockyLand clearingRockyLand;

    @Before
    public void setUp() {
        clearingRockyLand = new ClearingRockyLand();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(clearingRockyLand.isSuccessfulClearing()).isEqualTo(true);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfTwo() {
        assertThat(clearingRockyLand.getCostMap()).containsExactly(entry(CostItem.FUEL, 2));
    }
}