package com.aconex.siteclearing.activity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import com.aconex.siteclearing.cost.CostItem;
import org.junit.Before;
import org.junit.Test;

public class VisitingClearedLandTest {

    private VisitingClearedLand visitingClearedLand;

    @Before
    public void setUp() {
        visitingClearedLand = new VisitingClearedLand();
    }

    @Test
    public void isSuccessfulClearingShouldReturnTrue() {
        assertThat(visitingClearedLand.isSuccessfulClearing()).isEqualTo(false);
    }

    @Test
    public void getCostMapShouldReturnAMapWithOnlyFuelKeyAndValueOfOne() {
        assertThat(visitingClearedLand.getCostMap()).containsExactly(entry(CostItem.FUEL, 1));
    }
}