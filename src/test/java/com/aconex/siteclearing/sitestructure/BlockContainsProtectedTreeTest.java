package com.aconex.siteclearing.sitestructure;

import static org.assertj.core.api.Assertions.assertThat;

import com.aconex.siteclearing.activity.VisitingLandWithProtectedTree;
import org.junit.Before;
import org.junit.Test;

public class BlockContainsProtectedTreeTest {

    private BlockContainsProtectedTree blockContainsProtectedTree;

    @Before
    public void setUp() {
        blockContainsProtectedTree = new BlockContainsProtectedTree();
    }

    @Test
    public void getActivityShouldReturnVisitingLandWithProtectedTree() {
        assertThat(blockContainsProtectedTree.getActivity(false))
                .isInstanceOf(VisitingLandWithProtectedTree.class);
    }

}