package com.aconex.siteclearing.sitestructure;

import static org.assertj.core.api.Assertions.assertThat;

import com.aconex.siteclearing.activity.ClearingLandWithClearableTree;
import com.aconex.siteclearing.activity.PassingLandWithClearableTree;
import com.aconex.siteclearing.activity.VisitingClearedLand;
import org.junit.Before;
import org.junit.Test;

public class BlockContainsClearableTreeTest {

    private BlockContainsClearableTree blockContainsClearableTree;

    @Before
    public void setUp() {
        blockContainsClearableTree = new BlockContainsClearableTree();
    }

    @Test
    public void getActivityShouldReturnClearingLandWithClearableTreeWhenBlockWasNotClearedAndStoppedIn() {
        assertThat(blockContainsClearableTree.getActivity(true)).isInstanceOf(ClearingLandWithClearableTree.class);
    }

    @Test
    public void getActivityShouldReturnPassingLandWithClearableTreeWhenBlockWasNotClearedAndNotStoppedIn() {
        assertThat(blockContainsClearableTree.getActivity(false)).isInstanceOf(PassingLandWithClearableTree.class);
    }

    @Test
    public void getActivityShouldReturnVisitingClearedLandWhenBlockWasCleared() {
        blockContainsClearableTree.getActivity(true);
        assertThat(blockContainsClearableTree.getActivity(false)).isInstanceOf(VisitingClearedLand.class);
    }

}