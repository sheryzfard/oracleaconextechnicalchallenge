package com.aconex.siteclearing.sitestructure;

import static org.assertj.core.api.Assertions.assertThat;

import com.aconex.siteclearing.activity.ClearingRockyLand;
import com.aconex.siteclearing.activity.VisitingClearedLand;
import org.junit.Before;
import org.junit.Test;

public class RockyBlockTest {

    private RockyBlock rockyBlock;

    @Before
    public void setUp() {
        rockyBlock = new RockyBlock();
    }

    @Test
    public void shouldReturnClearingRockyLandWhenBlockWasNotVisited() {
        assertThat(rockyBlock.getActivity(false))
                .isInstanceOf(ClearingRockyLand.class);
    }

    @Test
    public void shouldReturnVisitingClearedLandWhenBlockWasNotVisited() {
        rockyBlock.getActivity(false);
        assertThat(rockyBlock.getActivity(false))
                .isInstanceOf(VisitingClearedLand.class);
    }

}