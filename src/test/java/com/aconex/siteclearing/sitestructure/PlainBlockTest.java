package com.aconex.siteclearing.sitestructure;

import static org.assertj.core.api.Assertions.assertThat;

import com.aconex.siteclearing.activity.ClearingPlainLand;
import org.junit.Before;
import org.junit.Test;

public class PlainBlockTest {

    private PlainBlock plainBlock;

    @Before
    public void setUp() {
        plainBlock = new PlainBlock();
    }

    @Test
    public void getActivityShouldReturnClearingPlainLand() {
        assertThat(plainBlock.getActivity(false))
                .isInstanceOf(ClearingPlainLand.class);
    }
}