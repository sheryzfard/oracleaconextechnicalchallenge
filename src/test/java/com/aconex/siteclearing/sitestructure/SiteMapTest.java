package com.aconex.siteclearing.sitestructure;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SiteMapTest {

    private SiteMap siteMap;

    @Before
    public void setUp() {
        List<String> strList = new ArrayList<>();
        strList.add("oootoor");
        strList.add("oorooTo");
        strList.add("ooootro");
        siteMap = new SiteMap(strList);
    }

    @Test
    public void getBlockShouldReturnPlainBLockForo() {
        assertThat(siteMap.getBlocks()[0][0]).isInstanceOf(PlainBlock.class);
    }

    @Test
    public void getBlockShouldReturnRockyBlockForr() {
        assertThat(siteMap.getBlocks()[6][0]).isInstanceOf(RockyBlock.class);
    }

    @Test
    public void getBlockShouldReturnBlockContainsClearableTreeFort() {
        assertThat(siteMap.getBlocks()[4][2]).isInstanceOf(BlockContainsClearableTree.class);
    }

    @Test
    public void getBlockShouldReturnBlockContainsProtectedTreeForT() {
        assertThat(siteMap.getBlocks()[5][1]).isInstanceOf(BlockContainsProtectedTree.class);
    }
}