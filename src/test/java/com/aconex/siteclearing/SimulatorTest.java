package com.aconex.siteclearing;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import com.aconex.siteclearing.cost.CostItem;
import com.aconex.siteclearing.cost.CostReporter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SimulatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private final static String INPUT_STREAM = "a 1\nq";

    private Simulator simulator;

    @Mock
    private MapReader mapReader;
    @Mock
    private CostReporter costReporter;
    private URI mapURI;

    @Before
    public void setUpStreams() {
        initMocks(this);
        simulator = new Simulator(mapReader, costReporter);
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }


    @Test
    public void runShouldPrintCostReport() {
        ByteArrayInputStream in = new ByteArrayInputStream(INPUT_STREAM.getBytes());
        System.setIn(in);
        Scanner input = new Scanner(in);
        when(mapReader.readMap(mapURI)).thenReturn(Arrays.asList("oo", "oo"));

        simulator.run(input, mapURI);

        HashMap<CostItem, Integer> costMap = new HashMap<>();
        costMap.put(CostItem.COMMUNICATION_OVERHEAD, 1);
        costMap.put(CostItem.FUEL, 1);
        costMap.put(CostItem.UNCLEARED_SQUARE, 3);
        costMap.put(CostItem.PROTECTED_TREE_DESTRUCTION, 0);
        costMap.put(CostItem.REPAIRING_PAINT_DAMAGE, 0);
        verify(costReporter).printCostReport(costMap);
    }

}