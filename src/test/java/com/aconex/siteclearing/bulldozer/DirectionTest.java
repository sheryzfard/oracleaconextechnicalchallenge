package com.aconex.siteclearing.bulldozer;

import static com.aconex.siteclearing.bulldozer.Direction.EAST;
import static com.aconex.siteclearing.bulldozer.Direction.NORTH;
import static com.aconex.siteclearing.bulldozer.Direction.WEST;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;


public class DirectionTest {

    @Test
    public void turnLeftShouldReturnWestWhenDirectionIsNorth() {
        assertThat(NORTH.getLeftTurn()).isEqualTo(WEST);
    }

    @Test
    public void turnRightShouldReturnEastWhenDirectionIsNorth() {
        assertThat(NORTH.getRightTurn()).isEqualTo(EAST);
    }
}