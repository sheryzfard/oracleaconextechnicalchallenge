package com.aconex.siteclearing.bulldozer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.aconex.siteclearing.ActivityCostManager;
import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.sitestructure.SiteMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class BulldozerTest {

    private Bulldozer bulldozer;
    @Mock
    private SiteMap siteMap;
    @Mock
    private ActivityCostManager activityCostManager;
    @Mock
    private Activity activity;

    @Before
    public void setUp() {
        initMocks(this);
        bulldozer = createBulldozerInTheMiddleOf3By3Site();
    }

    @Test
    public void moveForwardAndTryToCleanShouldVisitEastBlockWhenDirectionIsEast() {
        when(siteMap.getActivity(2, 1, true)).thenReturn(activity);

        bulldozer.moveForwardAndTryToClean(1);

        verify(activityCostManager).runActivity(activity);
    }

    @Test
    public void moveForwardAndTryToCleanShouldVisitWestBlockWhenDirectionIsWest() {
        when(siteMap.getActivity(0, 1, true)).thenReturn(activity);
        makeEastFacingBulldozerFaceWest(bulldozer);

        bulldozer.moveForwardAndTryToClean(1);

        verify(activityCostManager).runActivity(activity);
    }

    @Test
    public void moveForwardAndTryToCleanShouldVisitNorthBlockWhenDirectionIsNorth() {
        when(siteMap.getActivity(1, 0, true)).thenReturn(activity);
        makeEastFacingBulldozerFaceNorth(bulldozer);

        bulldozer.moveForwardAndTryToClean(1);

        verify(activityCostManager).runActivity(activity);
    }

    @Test
    public void moveForwardAndTryToCleanShouldVisitSouthBlockWhenDirectionIsSouth() {
        when(siteMap.getActivity(1, 2, true)).thenReturn(activity);
        makeEastFacingBulldozerFaceSouth(bulldozer);

        bulldozer.moveForwardAndTryToClean(1);

        verify(activityCostManager).runActivity(activity);
    }

    private void makeEastFacingBulldozerFaceSouth(Bulldozer bulldozer) {
        bulldozer.turnRight();
    }

    private void makeEastFacingBulldozerFaceNorth(Bulldozer bulldozer) {
        bulldozer.turnLeft();
    }

    private void makeEastFacingBulldozerFaceWest(Bulldozer bulldozer) {
        bulldozer.turnLeft();
        bulldozer.turnLeft();
    }


    private Bulldozer createBulldozerInTheMiddleOf3By3Site() {
        return new Bulldozer(1, 1, Direction.EAST, siteMap, activityCostManager);
    }
}