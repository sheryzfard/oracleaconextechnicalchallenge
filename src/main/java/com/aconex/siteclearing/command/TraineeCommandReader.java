package com.aconex.siteclearing.command;

public class TraineeCommandReader {

    public Command readCommand(String command) {
        if (command.charAt(0) == 'q') {
            return new Command(CommandType.QUIT);
        } else if (command.charAt(0) == 'a' && command.split(" ").length == 2) {
            try {
                int movementCount = Integer.parseInt(command.split(" ")[1]);
                return new Command(movementCount, CommandType.MOVE_FORWARD);
            }catch (NumberFormatException e) {
                return new Command(CommandType.WRONG_COMMAND);
            }
        } else if (command.charAt(0) == 'r') {
            return new Command(CommandType.TURN_RIGHT);
        } else if (command.charAt(0) == 'l') {
            return new Command(CommandType.TURN_LEFT);
        }
        else{
            return new Command(CommandType.WRONG_COMMAND);
        }
    }
}


