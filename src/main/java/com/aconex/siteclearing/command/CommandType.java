package com.aconex.siteclearing.command;

public enum CommandType {
    TURN_RIGHT("turn right"),TURN_LEFT("turn left"), MOVE_FORWARD("advance "), QUIT("quit"),WRONG_COMMAND("wrong command");

    private String description;

    CommandType(String description) {
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
