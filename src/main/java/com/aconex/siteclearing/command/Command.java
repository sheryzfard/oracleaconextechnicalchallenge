package com.aconex.siteclearing.command;

public class Command {

    private Integer movementNumber;
    private CommandType commandType;

    public Command(Integer movementNumber, CommandType commandType) {
        this.movementNumber = movementNumber;
        this.commandType = commandType;
    }

    public Command(CommandType commandType) {
        this.commandType = commandType;
    }

    public Integer getMovementNumber() {
        return movementNumber;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    @Override
    public String toString() {
        if(commandType.equals(CommandType.MOVE_FORWARD)){
            return commandType.getDescription() + movementNumber ;
        }
        else return commandType.getDescription();
    }
}
