package com.aconex.siteclearing.exceptions;

public class OutOfSiteBoundaryException extends RuntimeException {
    public OutOfSiteBoundaryException() {
        super("There is an attempt to navigate beyon the boundries of the site.");
    }
}
