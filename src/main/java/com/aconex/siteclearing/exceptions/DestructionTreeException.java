package com.aconex.siteclearing.exceptions;

public class DestructionTreeException extends RuntimeException {
    public DestructionTreeException() {
        super("there is an attempt to remove a tree that is protected. ");
    }
}
