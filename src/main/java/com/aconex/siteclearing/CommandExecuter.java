package com.aconex.siteclearing;

import com.aconex.siteclearing.bulldozer.Bulldozer;
import com.aconex.siteclearing.command.Command;

class CommandExecuter {

    private Bulldozer bulldozer;
    private ActivityCostManager activityCostManager;

    public CommandExecuter(Bulldozer bulldozer, ActivityCostManager activityCostManager) {
        this.bulldozer = bulldozer;
        this.activityCostManager = activityCostManager;
    }

    void executeCommand(Command command) {
        switch (command.getCommandType()) {
            case WRONG_COMMAND:
                System.out.println("the command format is wrong! try again.");
                break;
            case TURN_LEFT:
                activityCostManager.updateCommunicationCost();
                bulldozer.turnLeft();
                break;
            case TURN_RIGHT:
                activityCostManager.updateCommunicationCost();
                bulldozer.turnRight();
                break;
            case MOVE_FORWARD:
                activityCostManager.updateCommunicationCost();
                bulldozer.moveForwardAndTryToClean(command.getMovementNumber());
                break;
            case QUIT:
                System.out.print("the simulation has ended at your request. ");
        }

    }
}
