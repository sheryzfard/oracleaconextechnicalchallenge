package com.aconex.siteclearing.sitestructure;
import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.activity.ClearingLandWithClearableTree;
import com.aconex.siteclearing.activity.PassingLandWithClearableTree;
import com.aconex.siteclearing.activity.VisitingClearedLand;

public class BlockContainsClearableTree implements Block {

    private boolean isCleaned;

    public BlockContainsClearableTree(){
        isCleaned = Boolean.FALSE;
    }

    public Activity getActivity(Boolean stoppedIn) {
        if(!isCleaned) {
            isCleaned = Boolean.TRUE;
            if (stoppedIn) {
                return new  ClearingLandWithClearableTree();
            } else {
                return new PassingLandWithClearableTree();
            }
        }
        else {
            return new VisitingClearedLand();
        }
    }
}
