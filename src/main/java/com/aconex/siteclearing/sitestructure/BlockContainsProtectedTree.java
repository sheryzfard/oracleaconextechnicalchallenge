package com.aconex.siteclearing.sitestructure;
import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.activity.VisitingLandWithProtectedTree;

public class BlockContainsProtectedTree implements Block {

    public Activity getActivity(Boolean stoppedIn) {
        return new VisitingLandWithProtectedTree();
    }
}
