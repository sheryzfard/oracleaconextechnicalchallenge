package com.aconex.siteclearing.sitestructure;

import java.util.List;

import com.aconex.siteclearing.activity.Activity;

public class SiteMap {

    private int unCleanedBlocksNumber;
    private Block[][] blocks;

    public SiteMap(List<String> fileStream) {
        this.unCleanedBlocksNumber = 0;
        setBlocks(fileStream);
    }

    Block[][] getBlocks() {
        return blocks;
    }

    private void setBlocks(List<String> characterizedMap) {
        this.blocks = new Block[characterizedMap.get(0).length()][characterizedMap.size()];
        for (int j = 0; j < characterizedMap.size(); j++) {
            for (int i = 0; i < characterizedMap.get(j).length(); i++) {
                switch (characterizedMap.get(j).charAt(i)) {
                    case ('o'):
                        this.blocks[i][j] = new PlainBlock();
                        this.unCleanedBlocksNumber++;
                        break;
                    case ('r'):
                        this.blocks[i][j] = new RockyBlock();
                        this.unCleanedBlocksNumber++;
                        break;
                    case ('t'):
                        this.blocks[i][j] = new BlockContainsClearableTree();
                        this.unCleanedBlocksNumber++;
                        break;
                    case ('T'):
                        this.blocks[i][j] = new BlockContainsProtectedTree();
                        break;
                }
            }
        }
    }

    public int getUnCleanedBlocksNumber() {
        return unCleanedBlocksNumber;
    }

    public Activity getActivity(Integer x, int y, boolean bulldozerHasStoppedInBlock) {
        return blocks[x][y].getActivity(bulldozerHasStoppedInBlock);
    }
}
