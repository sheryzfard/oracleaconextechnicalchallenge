package com.aconex.siteclearing.sitestructure;
import com.aconex.siteclearing.activity.Activity;

public interface Block {

    Activity getActivity(Boolean stoppedIn);
}
