package com.aconex.siteclearing.sitestructure;

import com.aconex.siteclearing.activity.ClearingRockyLand;
import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.activity.VisitingClearedLand;

public class RockyBlock implements Block {

    private boolean isCleaned;

    public RockyBlock(){
        isCleaned = Boolean.FALSE;
    }


    public Activity getActivity(Boolean stoppedIn) {
        if(!isCleaned) {
            isCleaned = Boolean.TRUE;
            return new ClearingRockyLand();
        }
        else{
            return new VisitingClearedLand();
        }
    }
}
