package com.aconex.siteclearing.sitestructure;

import com.aconex.siteclearing.activity.ClearingPlainLand;
import com.aconex.siteclearing.activity.Activity;

public class PlainBlock implements Block {

    public Activity getActivity(Boolean stoppedIn) {
        return new ClearingPlainLand();
    }
}
