package com.aconex.siteclearing.cost;

import java.util.Map;

public class CostReporter {


    public void printCostReport(Map<CostItem,Integer> costMap){

        int totalCost = 0;

System.out.println();
        System.out.println("the costs for this land clearing operation were:\n") ;

        System.out.printf("%-30s %10s %10s", "Item", "Quantity", "Cost");
        System.out.println();for (Map.Entry<CostItem, Integer> entry : costMap.entrySet()) {
            System.out.format("%-30s %10s %10s",entry.getKey().getDescription() , entry.getValue() , (entry.getValue() * entry.getKey().getCostPerItem()));System.out.println();
            totalCost += (entry.getValue() * entry.getKey().getCostPerItem());
        }

        System.out.println("-------");
        System.out.printf("%-41s %10d %n","Total: " , totalCost);
        System.out.println("Thank you for using Aconex site clearing simulator.");

    }

}

