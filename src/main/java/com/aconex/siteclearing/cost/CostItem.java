package com.aconex.siteclearing.cost;

public enum CostItem {

    FUEL("fuel usage",1),
    COMMUNICATION_OVERHEAD("communication overhead",1),
    PROTECTED_TREE_DESTRUCTION("destruction of protected tree",10),
    REPAIRING_PAINT_DAMAGE("paint damage to bulldozer",2),
    UNCLEARED_SQUARE("uncleared squares",3);

    private String description;
    private int costPerItem;


    CostItem(String description, int costPerItem) {
        this.description = description;
        this.costPerItem = costPerItem;
    }

    public String getDescription() {
        return description;
    }

    public int getCostPerItem() {
        return costPerItem;
    }
}
