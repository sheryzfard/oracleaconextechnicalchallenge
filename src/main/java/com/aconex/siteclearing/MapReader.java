package com.aconex.siteclearing;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MapReader {

     List<String> readMap(URI mapFileUri) {
        try {
            return Files.readAllLines(Paths.get(mapFileUri));
        } catch (IOException e) {
            throw new RuntimeException("Not able to read map file", e);
        }
    }
}
