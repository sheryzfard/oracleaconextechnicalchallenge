package com.aconex.siteclearing.bulldozer;

import com.aconex.siteclearing.ActivityCostManager;
import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.exceptions.OutOfSiteBoundaryException;
import com.aconex.siteclearing.sitestructure.SiteMap;

public class Bulldozer {

    private int x;
    private int y;
    private Direction direction;
    private SiteMap siteMap;
    private ActivityCostManager activityCostManager;

    public Bulldozer(int x, int y, Direction direction, SiteMap siteMap, ActivityCostManager activityCostManager) {
        this.x = x;
        this.y = y;
        setDirection(direction);
        this.siteMap = siteMap;
        this.activityCostManager = activityCostManager;
    }

    private void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void turnLeft() {
        setDirection(direction.getLeftTurn());
    }

    public void turnRight() {
        setDirection(direction.getRightTurn());
    }

    public void moveForwardAndTryToClean(int movementCount) {
        try {
            switch (direction) {
                case EAST:
                    for (Integer i = x + 1; i <= x + movementCount; i++) {
                        boolean bulldozerHasStoppedInBlock = (i == (x + movementCount));
                        Activity activity = siteMap.getActivity(i, y, bulldozerHasStoppedInBlock);
                        activityCostManager.runActivity(activity);
                    }
                    x += movementCount;
                    break;
                case WEST:
                    for (Integer i = x - 1; i >= x - movementCount; i--) {
                        boolean bulldozerHasStoppedInBlock = i == (x - movementCount);
                        Activity activity = siteMap.getActivity(i, y, bulldozerHasStoppedInBlock);
                        activityCostManager.runActivity(activity);
                    }
                    x -= movementCount;
                    break;
                case NORTH:
                    for (Integer j = y - 1; j >= y - movementCount; j--) {
                        boolean bulldozerHasStoppedInBlock = (j == (y - movementCount));
                        Activity activity = siteMap.getActivity(x, j, bulldozerHasStoppedInBlock);
                        activityCostManager.runActivity(activity);
                    }
                    y -= movementCount;
                    break;
                case SOUTH:
                    for (Integer j = y + 1; j <= y + movementCount; j++) {
                        boolean bulldozerHasStoppedInBlock = (j == (y + movementCount));
                        Activity activity = siteMap.getActivity(x, j, bulldozerHasStoppedInBlock);
                        activityCostManager.runActivity(activity);
                    }
                    y += movementCount;
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new OutOfSiteBoundaryException();
        }
    }



}
