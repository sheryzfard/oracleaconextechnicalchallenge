package com.aconex.siteclearing.bulldozer;

public enum Direction {
    EAST, NORTH, WEST, SOUTH;

    private Direction leftTurn;
    private Direction rightTurn;

    static {
        setTurns(EAST, NORTH, SOUTH);
        setTurns(NORTH, WEST, EAST);
        setTurns(WEST, SOUTH, NORTH);
        setTurns(SOUTH, EAST, WEST);
    }

    private static void setTurns(Direction direction, Direction leftTurn, Direction rightTurn) {
        direction.leftTurn = leftTurn;
        direction.rightTurn = rightTurn;
    }

    public Direction getLeftTurn() {
        return leftTurn;
    }

    public Direction getRightTurn() {
        return rightTurn;
    }
}
