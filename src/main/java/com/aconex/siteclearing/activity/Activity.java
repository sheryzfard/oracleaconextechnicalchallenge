package com.aconex.siteclearing.activity;

import java.util.Map;

import com.aconex.siteclearing.cost.CostItem;

public interface Activity {

    boolean isSuccessfulClearing();

    Map<CostItem,Integer> getCostMap();

    void doAction();
}
