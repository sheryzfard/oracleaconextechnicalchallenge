package com.aconex.siteclearing.activity;

import java.util.HashMap;
import java.util.Map;

import com.aconex.siteclearing.cost.CostItem;
import com.aconex.siteclearing.exceptions.DestructionTreeException;

public class VisitingLandWithProtectedTree implements Activity {

    @Override
    public boolean isSuccessfulClearing(){
        return Boolean.FALSE;
    }

    @Override
    public Map<CostItem, Integer> getCostMap() {
        Map<CostItem, Integer> costMap = new HashMap<>();
        costMap.put(CostItem.FUEL,1);
        costMap.put(CostItem.PROTECTED_TREE_DESTRUCTION,1);
        return costMap;
    }

    @Override
    public void doAction() {
        throw new DestructionTreeException();
    }
}
