package com.aconex.siteclearing.activity;
import com.aconex.siteclearing.cost.CostItem;

import java.util.HashMap;
import java.util.Map;

public class ClearingPlainLand implements Activity {

    public boolean isSuccessfulClearing() {
        return Boolean.TRUE;
    }

    @Override
    public Map<CostItem, Integer> getCostMap() {
        Map<CostItem, Integer> costMap = new HashMap<>();
        costMap.put(CostItem.FUEL,1);
        return costMap;
    }

    @Override
    public void doAction() {}
}
