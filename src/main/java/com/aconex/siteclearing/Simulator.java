package com.aconex.siteclearing;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import com.aconex.siteclearing.bulldozer.Bulldozer;
import com.aconex.siteclearing.bulldozer.Direction;
import com.aconex.siteclearing.command.Command;
import com.aconex.siteclearing.command.CommandType;
import com.aconex.siteclearing.command.TraineeCommandReader;
import com.aconex.siteclearing.cost.CostReporter;
import com.aconex.siteclearing.sitestructure.SiteMap;

public class Simulator {

    private final static String WELCOME_MESSAGE = "Welcome to the Aconex site clearing simulator. This map is a map of the site:\n\n";
    private final static String BULLDOZER_LOCATION_INFO = "The bulldozer is currently located an the Northern edge of the site, immediately to the west of the site and facing East.\n\n";
    private final static String COMMAND_GUID_MESSAGE = "(l)eft, r)ight, a)dvance <n>, q)uit: ";
    private final static String SHOW_COMMAND_MESSAGE = "These are the commands you issued:\n\n";

    private MapReader mapReader;
    private CostReporter costReporter;

    private SiteMap siteMap;
    private ActivityCostManager activityCostManager;
    private TraineeCommandReader commandReader;
    private CommandExecuter commandExecuter;
    private List<String> commandList;

    public Simulator(MapReader mapReader, CostReporter costReporter) {
        this.mapReader = mapReader;
        this.costReporter = costReporter;
    }

    private void buildObjects() {
        activityCostManager = new ActivityCostManager(new TreeMap<>(), siteMap.getUnCleanedBlocksNumber());
        Bulldozer bulldozer = new Bulldozer(-1, 0, Direction.EAST, siteMap, activityCostManager);
        commandReader = new TraineeCommandReader();
        commandExecuter = new CommandExecuter(bulldozer, activityCostManager);
        commandList = new ArrayList<>();
    }

    private void print(String msg) {
        System.out.print(msg);
    }

    public void run(Scanner scanner, URI mapFileURI) {

        buildSiteMap(mapFileURI);
        buildObjects();
        print(BULLDOZER_LOCATION_INFO);
        try {
            Command command;
            do {
                print(COMMAND_GUID_MESSAGE);
                command = commandReader.readCommand(scanner.nextLine());
                commandExecuter.executeCommand(command);
                commandList.add(command.toString());
            } while (!command.getCommandType().equals(CommandType.QUIT));
        } catch (Exception e) {
            print(e.getMessage());
        } finally {
            print(SHOW_COMMAND_MESSAGE);
            System.out.println(commandList);
            costReporter.printCostReport(activityCostManager.getCostMap());
        }
    }

    private void buildSiteMap(URI mapFileURI) {
        List<String> map = mapReader.readMap(mapFileURI);
        print(WELCOME_MESSAGE);
        for (String mapLine : map) {
            System.out.println(mapLine);
        }
        System.out.println();
        siteMap = new SiteMap(map);
    }
}
