package com.aconex.siteclearing;

import java.util.Map;

import com.aconex.siteclearing.activity.Activity;
import com.aconex.siteclearing.cost.CostItem;

public class ActivityCostManager {
    private Map<CostItem, Integer> costMap;

     ActivityCostManager(Map<CostItem,Integer> costMap, int unClearedSquares){
        this.costMap = costMap;
        this.costMap.put(CostItem.COMMUNICATION_OVERHEAD,0);
        this.costMap.put(CostItem.FUEL, 0);
        this.costMap.put(CostItem.UNCLEARED_SQUARE,unClearedSquares);
        this.costMap.put(CostItem.PROTECTED_TREE_DESTRUCTION,0);
        this.costMap.put(CostItem.REPAIRING_PAINT_DAMAGE,0);
    }

    public void runActivity(Activity activity) {
        update(activity.getCostMap());
        activity.doAction();
        if(activity.isSuccessfulClearing()){
            updateUnclearedCost();
        }
    }
    void updateCommunicationCost(){
        updateQuantityOfCostItemInCostDetail(CostItem.COMMUNICATION_OVERHEAD,1);
    }

    Map<CostItem, Integer> getCostMap(){
        return costMap;
    }

    private void updateUnclearedCost(){
        updateQuantityOfCostItemInCostDetail(CostItem.UNCLEARED_SQUARE,-1);
    }

    private void update(Map<CostItem,Integer> costMap){
        for (Map.Entry<CostItem,Integer> entry:costMap.entrySet()){
            updateQuantityOfCostItemInCostDetail(entry.getKey(),entry.getValue());
        }
    }

    private void updateQuantityOfCostItemInCostDetail(CostItem costItem, int quantity){
        costMap.replace(costItem,costMap.get(costItem)+quantity);
    }
}
