import java.net.URI;
import java.nio.file.Paths;
import java.util.Scanner;

import com.aconex.siteclearing.MapReader;
import com.aconex.siteclearing.Simulator;
import com.aconex.siteclearing.cost.CostReporter;

public class Main {

    public static void main(String[] args) {

        MapReader mapReader = new MapReader();
        CostReporter costReporter = new CostReporter();

        Simulator simulator = new Simulator(mapReader, costReporter);
        if (args.length == 1) {
            URI mapURI = Paths.get(args[0]).normalize().toUri();
            Scanner scanner = new Scanner(System.in);
            simulator.run(scanner, mapURI);
        } else {
            throw new IllegalArgumentException("Application should have one argument as file map");
        }
    }
}
